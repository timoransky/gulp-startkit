var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

// browserSync

gulp.task('browserSync', function() {
	browserSync.init({
		server: {
			baseDir: "./",
		},
	})
});

// SASS COMPILE
gulp.task('sass',function() {
	return gulp.src('assets/scss/style.scss')
	.pipe(sourcemaps.init())
	.pipe(sass())
	.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: false
	}))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets/css'))
	.pipe(browserSync.reload({
		stream: true
	}))
});



// WATCH
gulp.task('watch',['browserSync', 'sass'],function() {
	gulp.watch('assets/scss/**',['sass']);
	gulp.watch('./*.html',browserSync.reload);
	gulp.watch('assets/js/*.js',browserSync.reload);
	gulp.watch('assets/img/**',browserSync.reload);
})

